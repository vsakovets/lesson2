package com.svs.sakovets_l2_t2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));
        persons.add(new Person(R.drawable.norris, "Чак Норрис", "вечно молодой"));



        PersonAdapter personAdapter = new PersonAdapter(persons);

    recyclerView.setAdapter(personAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }


}

